from setuptools import setup, find_packages

pkj_name = 'vuejs_translate'

with open('README.rst') as f:
    readme = f.read()

setup(
    name='django-vuejs-translate',
    version='0.3',
    install_requires=[
        'django',
        'polib'
    ],
    packages=[pkj_name] + [pkj_name + '.' + x for x in find_packages(pkj_name)],
    url='https://gitlab.com/cyberbudy/django-vuejs-translate',
    license='MIT',
    author='cyberbudy',
    author_email='cyberbudy@gmail.com',
    description='Generating translations from strings inside vuejs',
    long_description=readme,
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3'
    ]

)
